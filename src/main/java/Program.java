import java.util.Arrays;
import java.util.stream.Collectors;

public class Program {
    public static void main(String args[])
    {
        StringBuilder builder = new StringBuilder("Hello World.");
        if(0 < args.length)
        {
            builder.append(" args: ");
        }

        builder.append(Arrays.asList(args).stream().collect(Collectors.joining(",")));
        System.out.println(builder.toString());
    }
}
